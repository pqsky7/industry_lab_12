package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by qpen546 on 11/04/2017.
 */
public class GemShape extends Shape {
    public GemShape() {
        super();
    }

    /**
     * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
     * variables.
     *
     * @param x      x position.
     * @param y      y position.
     * @param deltaX speed and direction for horizontal axis.
     * @param deltaY speed and direction for vertical axis.
     */
    public GemShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    /**
     * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
     * variables.
     *
     * @param x      x position.
     * @param y      y position.
     * @param deltaX speed (pixels per move call) and direction for horizontal
     *               axis.
     * @param deltaY speed (pixels per move call) and direction for vertical
     *               axis.
     * @param width  width in pixels.
     * @param height height in pixels.
     */
    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    /**
     * Paints this ictgradschool.industry.lab12.bounce.RectangleShape object using the supplied ictgradschool.industry.lab12.bounce.Painter object.
     */
    @Override
    public void paint(Painter painter) {

        if (fWidth <= 40) {
            int[] x= {fWidth/2+fX,  fWidth+fX,     fWidth/2+fX,   0+fX};
            int[] y = {0+fY,         fHeight/2+fY,  fHeight+fY,    fHeight/2+fY};
            painter.drawPolygon(new Polygon(x,y,4));
        } else {
            int[] x ={20+fX,  fWidth-20+fX,     fWidth+fX,       fWidth-20+fX,  20+fX,         0+fX};
            int[] y ={0+fY,   0+fY,             fHeight/2+fY,    fHeight+fY,    fHeight+fY,    fHeight/2+fY};

            painter.drawPolygon(new Polygon(x,y,6));
        }
    }
}
