package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by qpen546 on 11/04/2017.
 */
public class DynamicShape extends Shape {
    private int pDeltaX;
    private int pDeltaY;
    private Color color = Color.BLACK;
    private int option = 1;

    public DynamicShape() {
        super();
    }

    /**
     * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
     * variables.
     *
     * @param x      x position.
     * @param y      y position.
     * @param deltaX speed and direction for horizontal axis.
     * @param deltaY speed and direction for vertical axis.
     */
    public DynamicShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
        pDeltaX = deltaX;
        pDeltaY = deltaY;
    }

    /**
     * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
     * variables.
     *
     * @param x      x position.
     * @param y      y position.
     * @param deltaX speed (pixels per move call) and direction for horizontal
     *               axis.
     * @param deltaY speed (pixels per move call) and direction for vertical
     *               axis.
     * @param width  width in pixels.
     * @param height height in pixels.
     */
    public DynamicShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
        pDeltaX = deltaX;
        pDeltaY = deltaY;
    }

    /**
     * Paints this ictgradschool.industry.lab12.bounce.RectangleShape object using the supplied ictgradschool.industry.lab12.bounce.Painter object.
     */

    public DynamicShape(int x, int y, int deltaX, int deltaY, Color color) {
        super(x, y, deltaX, deltaY);
        pDeltaX = deltaX;
        pDeltaY = deltaY;
        this.color = color;
    }

    @Override
    public void paint(Painter painter) {
        if (pDeltaX == fDeltaX && pDeltaY == fDeltaY) {
            paintMethod(painter,option);
        } else if (pDeltaX != fDeltaX&&pDeltaY==fDeltaY) {
            paintMethod(painter,1);
            option = 1;
        } else {
            paintMethod(painter,2);
            option = 2;
        }
        pDeltaX = fDeltaX;
        pDeltaY = fDeltaY;
    }

    public void paintMethod(Painter painter, int option) {

        if (option==1) {
            painter.setColor(Color.black);
            painter.drawRect(fX, fY, fWidth, fHeight);
        }else{
            painter.setColor(color);
            painter.fillRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.black);
            painter.drawRect(fX, fY, fWidth, fHeight);
        }
    }
}
