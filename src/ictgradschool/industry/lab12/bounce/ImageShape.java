package ictgradschool.industry.lab12.bounce;

import javax.imageio.ImageIO;
import javax.xml.transform.Source;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by qpen546 on 11/04/2017.
 */
public class ImageShape extends Shape{

    private BufferedImage image;
    private int width;
    private int height;
    private String picture;
    private Image scaledImage;

    public void imageInput() {
        try {
            image = ImageIO.read(new File(picture));
            super.fWidth = (int)(image.getWidth(null)*0.1);
            super.fHeight = (int)(image.getHeight(null)*0.1);
            scaledImage = image.getScaledInstance(fWidth,fHeight, Image.SCALE_DEFAULT);
        } catch (IOException exception) {
            System.out.println("Wrong filename or path");
        }
    }

    public ImageShape() {
            super();
        }


    /**
     * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
     * variables.
     *
     * @param x      x position.
     * @param y      y position.
     * @param deltaX speed and direction for horizontal axis.
     * @param deltaY speed and direction for vertical axis.
     */
    public ImageShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    /**
     * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
     * variables.
     *
     * @param x      x position.
     * @param y      y position.
     * @param deltaX speed (pixels per move call) and direction for horizontal
     *               axis.
     * @param deltaY speed (pixels per move call) and direction for vertical
     *               axis.
     * @param width  width in pixels.
     * @param height height in pixels.
     */
    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height,String source) {
        super(x, y, deltaX, deltaY, width, height);
        picture = source;
        imageInput();
    }
    public ImageShape(int x, int y, int deltaX, int deltaY,String source) {
        super(x, y, deltaX, deltaY);
        picture = source;
        imageInput();
    }
    /**
     * Paints this ictgradschool.industry.lab12.bounce.RectangleShape object using the supplied ictgradschool.industry.lab12.bounce.Painter object.
     */
    @Override
    public void paint(Painter painter) {

        painter.drawImage(scaledImage,fX,fY,null);
    }

}
